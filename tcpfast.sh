#!/usr/bin/env bash
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
export PATH

# =================================================
#	System Required: CentOS 6/7/8,Debian 8/9/10,ubuntu 16/18/19
#	Description: BBR+BBRplus+Lotserver
#	Version: 1.3.2.57
# Author: Qianying,cx9208,YLX
# Update content and feedback: https://blog.ylx.me/archives/783.html
# =================================================

sh_ver="1.3.2.57"
github="github.000060000.xyz"

Green_font_prefix="\033[32m" && Red_font_prefix="\033[31m" && Green_background_prefix="\033[42;37m" && Red_background_prefix="\033[41;37m" && Font_color_suffix="\033[0m"
Info="${Green_font_prefix}[信息]${Font_color_suffix}"
Error="${Red_font_prefix}[错误]${Font_color_suffix}"
Tip="${Green_font_prefix}[注意]${Font_color_suffix}"

#Install BBR kernel
installbbr(){
	kernel_version="5.9.6"
	bit=`uname -m`
	rm -rf bbr
	mkdir bbr && cd bbr
	
	if [[ "${release}" == "centos" ]]; then
		# if [[ ${version} = "6" ]]; then
			# if [[ ${bit} = "x86_64" ]]; then
				# wget -N -O kernel-headers-c6.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EUCmObDQnMZEmKnhxS67sJkBG8kjbx0bjNF-XwTtzvgtAA?download=1
				# wget -N -O kernel-c6.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EeC72joP3HVNmrIbjlPg_coBs7kj29Md4f9psAjZOuqOdg?download=1
			
				# yum install -y kernel-c6.rpm
				# yum install -y kernel-headers-c6.rpm
			
				# kernel_version="5.5.5"
			# else
				# echo -e "${Error} is still using 32-bit kernel, don't see you again!" && exit 1
			# fi
		
		if [[ ${version} = "7" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				kernel_version="5.10.0"
				detele_kernel_head
				wget -N -O kernel-headers-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EfW6q9Kun79Juwa9tyXK47cBJaL00gEmKNSBmhHl5Ubjjg?download=1
				wget -N -O kernel-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/ESTQpFw24rBIqfPRCEl9OuQBJS-2CQOOVm9BkeOgt3obcA?download=1
				
				yum install -y kernel-c7.rpm
				yum install -y kernel-headers-c7.rpm
			else
				echo -e "${Error} is still using 32-bit kernel, don't see you again!" && exit 1
			be	
			
	elif [[ ${version} = "8" ]]; then
			kernel_version="5.6.15"
			detele_kernel_head
			wget -N -O kernel-c8.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/ETadaTIeeQJCgxEXKlOFiCEBsBa-Y15QbDkv-HQGo2EHSQ?download=1
			wget -N -O kernel-headers-c8.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EZEZyLBjDplMgSqDzyaqkvYBW06OOKDCcIQq27381fa5-A?download=1

			yum install -y kernel-c8.rpm
			yum install -y kernel-headers-c8.rpm
		be
	
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		if [[ ${bit} = "x86_64" ]]; then
			kernel_version="5.10.0"
			detele_kernel_head
			wget -N -O linux-headers-d10.deb https://chinagz2018-my.sharepoint.com/:u:///personal/ylx_chinagz2018_onmicrosoft_com/EX3SCMEaZ7JLurzVauRfOKoBY1eiJSAOooEJQahw4dW=
			wget -N -O linux-image-d10.deb https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EdGQ0poVAfdLn0X1CA_z4eUBnx3Wx7qDqZAXKN1L2EapZw
				
			dpkg -i linux-image-d10.deb
			dpkg -i linux-headers-d10.deb
		else
			echo -e "${Error} is still using 32-bit kernel, don't see you again!" && exit 1	
		be	
	be
	
	cd .. && rm -rf bbr	
	
	detele_kernel
	BBR_grub
	echo -e "${Tip} ${Red_font_prefix} Please check whether there is kernel information on it, don’t restart ${Font_color_suffix} without a kernel
	echo -e "${Tip} ${Red_font_prefix}rescue is not a normal kernel, please exclude this ${Font_color_suffix}"
	echo -e "${Tip} After restarting the VPS, please re-run the script to enable ${Red_font_prefix}BBR${Font_color_suffix}"	
	stty erase'^H' && read -p "The VPS needs to be restarted before BBR can be turned on. Do you want to restart now? [Y/n] :" yn
	[ -z "${yn}" ] && yn="y"
	if [[$ yn == [Yy]]]; then
		echo -e "${Info} VPS is restarting..."
		reboot
	be
	#echo -e "${Tip} kernel installation is complete, please refer to the above information to check whether the installation is successful and manually adjust the kernel boot sequence"
}

#Install BBRplus kernel 4.14.129
installbbrplus(){
	kernel_version="4.14.160-bbrplus"
	bit=`uname -m`
	rm -rf bbrplus
	mkdir bbrplus && cd bbrplus
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O kernel-headers-c7.rpm https://github.com/cx9208/Linux-NetSpeed/raw/master/bbrplus/centos/7/kernel-headers-4.14.129-bbrplus.rpm
				wget -N -O kernel-c7.rpm https://github.com/cx9208/Linux-NetSpeed/raw/master/bbrplus/centos/7/kernel-4.14.129-bbrplus.rpm
				
				yum install -y kernel-c7.rpm
				yum install -y kernel-headers-c7.rpm
				
				kernel_version="4.14.129_bbrplus"
				detele_kernel_head
				
			else
					echo -e "${Error} is still using 32-bit kernel, don't see you again!" && exit 1
			be
		be	
		
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		wget -N -O linux-headers.deb https://github.com/cx9208/Linux-NetSpeed/raw/master/bbrplus/debian-ubuntu/x64/linux-headers-4.14.129-bbrplus.deb
		wget -N -O linux-image.deb https://github.com/cx9208/Linux-NetSpeed/raw/master/bbrplus/debian-ubuntu/x64/linux-image-4.14.129-bbrplus.deb
		
		dpkg -i linux-image.deb
		dpkg -i linux-headers.deb
			
		kernel_version="4.14.129-bbrplus"
	be
	
	cd .. && rm -rf bbrplus
	detele_kernel
	BBR_grub
	echo -e "${Tip} ${Red_font_prefix} Please check whether there is kernel information on it, don’t restart ${Font_color_suffix} without a kernel
	echo -e "${Tip} ${Red_font_prefix}rescue is not a normal kernel, please exclude this ${Font_color_suffix}"
	echo -e "${Tip} After restarting the VPS, please re-run the script to enable ${Red_font_prefix}BBRplus${Font_color_suffix}"
	stty erase'^H' && read -p "You need to restart the VPS before you can start BBRplus. Do you want to restart it now? [Y/n] :" yn
	[ -z "${yn}" ] && yn="y"
	if [[$ yn == [Yy]]]; then
		echo -e "${Info} VPS is restarting..."
		reboot
	be
	#echo -e "${Tip} kernel installation is complete, please refer to the above information to check whether the installation is successful and manually adjust the kernel boot sequence"
}

#Install Lotserver kernel
installlot(){
	if [[ "${release}" == "centos" ]]; then
		rpm --import http://${github}/lotserver/${release}/RPM-GPG-KEY-elrepo.org
		yum remove -y kernel-firmware
		yum install -y http://${github}/lotserver/${release}/${version}/${bit}/kernel-firmware-${kernel_version}.rpm
		yum install -y http://${github}/lotserver/${release}/${version}/${bit}/kernel-${kernel_version}.rpm
		yum remove -y kernel-headers
		yum install -y http://${github}/lotserver/${release}/${version}/${bit}/kernel-headers-${kernel_version}.rpm
		yum install -y http://${github}/lotserver/${release}/${version}/${bit}/kernel-devel-${kernel_version}.rpm
	elif [[ "${release}" == "ubuntu" ]]; then
		bash <(wget -qO- "https://${github}/Debian_Kernel.sh")
	elif [[ "${release}" == "debian" ]]; then
		bash <(wget -qO- "https://${github}/Debian_Kernel.sh")
	be
	
	detele_kernel
	BBR_grub
	echo -e "${Tip} ${Red_font_prefix} Please check whether there is kernel information on it, don’t restart ${Font_color_suffix} without a kernel
	echo -e "${Tip} ${Red_font_prefix}rescue is not a normal kernel, please exclude this ${Font_color_suffix}"
	echo -e "${Tip} After restarting the VPS, please re-run the script to enable ${Red_font_prefix}Lotserver${Font_color_suffix}"
	stty erase'^H' && read -p "You need to restart the VPS before you can start Lotserver. Do you want to restart it now? [Y/n] :" yn
	[ -z "${yn}" ] && yn="y"
	if [[$ yn == [Yy]]]; then
		echo -e "${Info} VPS is restarting..."
		reboot
	be
	#echo -e "${Tip} kernel installation is complete, please refer to the above information to check whether the installation is successful and manually adjust the kernel boot sequence"
}

#Install xanmod kernel from xanmod.org
installxanmod(){
	kernel_version="5.5.1-xanmod1"
	bit=`uname -m`
	rm -rf xanmod
	mkdir xanmod && cd xanmod
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				kernel_version="5.9.6_xanmod"
				detele_kernel_head
				wget -N -O kernel-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/ETLw9F6MgjBBlkkNGmFZu70B9p0kMOdqrF6ntv2QNI5I4g?download=1
				wget -N -O kernel-headers-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EcFnSZfa6JBKqZOkE5xFd8oBPJCa6Lo7DzTPPM0INWsk-w?download=1
				
				yum install -y kernel-c7.rpm
				yum install -y kernel-headers-c7.rpm			
			else
				echo -e "${Error} is still using 32-bit kernel, don't see you again!" && exit 1
			be
		elif [[ ${version} = "8" ]]; then
				kernel_version="5.5.1_xanmod1"
				detele_kernel_head
				wget -N -O kernel-c8.rpm https://github.com/ylx2016/kernel/releases/download/5.5.1xanmod/kernel-5.5.1_xanmod1-1-c8.x86_64.rpm
				wget -N -O kernel-headers-c8.rpm https://github.com/ylx2016/kernel/releases/download/5.5.1xanmod/kernel-headers-5.5.1_xanmod1-1-c8.x86_64.rpm
				
				yum install -y kernel-c8.rpm
				yum install -y kernel-headers-c8.rpm
		be
		
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		if [[ ${bit} = "x86_64" ]]; then
			kernel_version="5.9.13-xanmod"
			detele_kernel_head
			wget -N -O linux-headers-d10.deb https://chinagz2018-my.sharepoint.com/:u:///personal/ylx_chinagz2018_onmicrosoft_com/Ee_I3gDsGHFDvkUL0EXBSR4BKCvJYwJSdCC?
			wget -N -O linux-image-d10.deb https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EXzhrueMBNVChTNkkeyWzf0BKQBSIKIfL-RzEYiCV1JUgg?download=
				
			dpkg -i linux-image-d10.deb
			dpkg -i linux-headers-d10.deb
		else
			echo -e "${Error} is still using 32-bit kernel, don't see you again!" && exit 1	
		be		
	be
	
	cd .. && rm -rf xanmod
	detele_kernel
	BBR_grub
	echo -e "${Tip} ${Red_font_prefix} Please check whether there is kernel information on it, don’t restart ${Font_color_suffix} without a kernel
	echo -e "${Tip} ${Red_font_prefix}rescue is not a normal kernel, please exclude this ${Font_color_suffix}"
	echo -e "${Tip} After restarting the VPS, please re-run the script to enable ${Red_font_prefix}BBR${Font_color_suffix}"
	stty erase'^H' && read -p "The VPS needs to be restarted before BBR can be turned on. Do you want to restart now? [Y/n] :" yn
	[ -z "${yn}" ] && yn="y"
	if [[$ yn == [Yy]]]; then
		echo -e "${Info} VPS is restarting..."
		reboot
	be
	#echo -e "${Tip} kernel installation is complete, please refer to the above information to check whether the installation is successful and manually adjust the kernel boot sequence"
}

#Install bbr2 kernel
installbbr2(){
	kernel_version="5.4.0-rc6"
	bit=`uname -m`
	rm -rf bbr2
	mkdir bbr2 && cd bbr2
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O kernel-c7.rpm https://github.com/ylx2016/kernel/releases/download/5.4.0r6bbr2/kernel-5.4.0_rc6-1-bbr2-c7.x86_64.rpm
				wget -N -O kernel-headers-c7.rpm https://github.com/ylx2016/kernel/releases/download/5.4.0r6bbr2/kernel-headers-5.4.0_rc6-1-bbr2-c7.x86_64.rpm
				
				yum install -y kernel-c7.rpm
				yum install -y kernel-headers-c7.rpm
			
				kernel_version="5.4.0_rc6"
			else
				echo -e "${Error} is still using 32-bit kernel, don't see you again!" && exit 1
			be
		elif [[ ${version} = "8" ]]; then
				wget -N -O kernel-c8.rpm https://github.com/ylx2016/kernel/releases/download/5.4.0r6bbr2/kernel-5.4.0_rc6-1-bbr2-c8.x86_64.rpm
				wget -N -O kernel-headers-c8.rpm https://github.com/ylx2016/kernel/releases/download/5.4.0r6bbr2/kernel-headers-5.4.0_rc6-1-bbr2-c8.x86_64.rpm
				
				yum install -y kernel-c8.rpm
				yum install -y kernel-headers-c8.rpm
			
				kernel_version="5.4.0_rc6"
		be
		
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		if [[ ${bit} = "x86_64" ]]; then
			wget -N -O linux-headers-d10.deb https://github.com/ylx2016/kernel/releases/download/5.4.0r6bbr2/linux-headers-5.4.0-rc6_5.4.0-rc6-1-bbr2- d10_amd64.deb
				wget -N -O linux-image-d10.deb https://github.com/ylx2016/kernel/releases/download/5.4.0r6bbr2/linux-image-5.4.0-rc6_5.4.0-rc6-1-bbr2- d10_amd64.deb
					
				dpkg -i linux-image-d10.deb
				dpkg -i linux-headers-d10.deb
				
			# kernel_version="5.4.0_rc6"
		else
			echo -e "${Error} is still using 32-bit kernel, don't see you again!" && exit 1	
		be		
	be
	
	cd .. && rm -rf bbr2
	detele_kernel
	BBR_grub
	echo -e "${Tip} ${Red_font_prefix} Please check whether there is kernel information on it, don’t restart ${Font_color_suffix} without a kernel
	echo -e "${Tip} ${Red_font_prefix}rescue is not a normal kernel, please exclude this ${Font_color_suffix}"
	echo -e "${Tip} After restarting the VPS, please re-run the script to enable ${Red_font_prefix}BBR2${Font_color_suffix}"
	stty erase'^H' && read -p "You need to restart the VPS before you can turn on BBR2. Do you want to restart it now? [Y/n] :" yn
	[ -z "${yn}" ] && yn="y"
	if [[$ yn == [Yy]]]; then
		echo -e "${Info} VPS is restarting..."
		reboot
	be
	#echo -e "${Tip} kernel installation is complete, please refer to the above information to check whether the installation is successful and manually adjust the kernel boot sequence"
}

#Install Zen Kernel
installzen(){
	kernel_version="5.5.2-zen"
	bit=`uname -m`
	rm -rf zen
	mkdir zen && cd zen
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O kernel-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EfQb4N8c2bxDlF3mj3SBVHIBGFSg_d1uR4LFzzT0Ii5FWA?download=1
				wget -N -O kernel-headers-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EfKgMa8vsZBOt0zwXM_lHcUBOYlyH1CyRHrYSRJ5r6a0EQ?download=1
				
				yum install -y kernel-c7.rpm
				yum install -y kernel-headers-c7.rpm
			
				kernel_version="5.5.10_zen"
			else
				echo -e "${Error} is still using 32-bit kernel, don't see you again!" && exit 1
			be
		elif [[ ${version} = "8" ]]; then
				wget -N -O kernel-c8.rpm https://github.com/ylx2016/kernel/releases/download/5.5.2zen/kernel-5.5.2_zen-1-c8.x86_64.rpm
				wget -N -O kernel-headers-c8.rpm https://github.com/ylx2016/kernel/releases/download/5.5.2zen/kernel-headers-5.5.2_zen-1-c8.x86_64.rpm
				
				yum install -y kernel-c8.rpm
				yum install -y kernel-headers-c8.rpm
			
				kernel_version="5.5.2_zen"
		be
		
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		if [[ ${bit} = "x86_64" ]]; then
			wget -N -O linux-headers-d10.deb https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EShzFq8Jlv1PthbYlNNvLjIB2-hktrkPXxwd1g?
				wget -N -O linux-image-d10.deb https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/ERXzOc-2BzJInOxBgKo62OkBgcI9-O-fw0M8U2B4NazuLg?
					
				dpkg -i linux-image-d10.deb
				dpkg -i linux-headers-d10.deb
				
			kernel_version="5.5.10-zen"	
		else
			echo -e "${Error} is still using 32-bit kernel, don't see you again!" && exit 1	
		be		
	be
	
	cd .. && rm -rf zen
	detele_kernel
	BBR_grub
	echo -e "${Tip} ${Red_font_prefix} Please check whether there is kernel information on it, don’t restart ${Font_color_suffix} without a kernel
	echo -e "${Tip} ${Red_font_prefix}rescue is not a normal kernel, please exclude this ${Font_color_suffix}"
	echo -e "${Tip} After restarting the VPS, please re-run the script to enable ${Red_font_prefix}BBR${Font_color_suffix}"
	stty erase'^H' && read -p "The VPS needs to be restarted before BBR can be turned on. Do you want to restart now? [Y/n] :" yn
	[ -z "${yn}" ] && yn="y"
	if [[$ yn == [Yy]]]; then
		echo -e "${Info} VPS is restarting..."
		reboot
	be
	#echo -e "${Tip} kernel installation is complete, please refer to the above information to check whether the installation is successful and manually adjust the kernel boot sequence"
}

#Install bbrplus new kernel
installbbrplusnew(){
	kernel_version="4.14.182-bbrplus"
	bit=`uname -m`
	rm -rf bbrplusnew
	mkdir bbrplusnew && cd bbrplusnew
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" ]]; then
			if [[ ${bit} = "x86_64" ]]; then
				wget -N -O kernel-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EWtxHt1RiAlHgqERl5bvYzcBUrkKa_n1mWQ-uM2-Na7gmQ?download=1
				wget -N -O kernel-headers-c7.rpm https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EYkNoi17pKJBi7KnhUGRqEIBEK_26-bzkCL-fuQYZmrHWA?download=1
				
				yum install -y kernel-c7.rpm
				yum install -y kernel-headers-c7.rpm
			
				kernel_version="4.14.182_bbrplus"
			else
				echo -e "${Error} is still using 32-bit kernel, don't see you again!" && exit 1
			be
		be
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		if [[ ${bit} = "x86_64" ]]; then
			wget -N -O linux-headers-d10.deb https://chinagz2018-my.sharepoint.com/:u:///personal/ylx_chinagz2018_onmicrosoft_com/Ef9pJn1wp-pBk4FIPxT1qBoBqpWhTVCawvKM?
			wget -N -O linux-image-d10.deb https://chinagz2018-my.sharepoint.com/:u:/g/personal/ylx_chinagz2018_onmicrosoft_com/EaFJshr8za9Bq9FGjEBLds0B4ZfrYThLH8E35xe9-qWX_Q?
					
			dpkg -i linux-image-d10.deb
			dpkg -i linux-headers-d10.deb
				
			kernel_version="4.14.182-bbrplus"
		else
			echo -e "${Error} is still using 32-bit kernel, don't see you again!" && exit 1
		be
	be

	cd .. && rm -rf bbrplusnew
	detele_kernel
	BBR_grub
	echo -e "${Tip} ${Red_font_prefix} Please check whether there is kernel information on it, don’t restart ${Font_color_suffix} without a kernel
	echo -e "${Tip} ${Red_font_prefix}rescue is not a normal kernel, please exclude this ${Font_color_suffix}"
	echo -e "${Tip} After restarting the VPS, please re-run the script to enable ${Red_font_prefix}BBRplus${Font_color_suffix}"
	stty erase'^H' && read -p "You need to restart the VPS before you can start BBRplus. Do you want to restart it now? [Y/n] :" yn
	[ -z "${yn}" ] && yn="y"
	if [[$ yn == [Yy]]]; then
		echo -e "${Info} VPS is restarting..."
		reboot
	be
	#echo -e "${Tip} kernel installation is complete, please refer to the above information to check whether the installation is successful and manually adjust the kernel boot sequence"

}

#Enable BBR+fq
startbbrfq () {
	remove_all
	echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBR+FQ modified successfully, restart to take effect!"
}

#Enable BBR+fq_pie
startbbrfqpie () {
	remove_all
	echo "net.core.default_qdisc=fq_pie" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBR+FQ_PIE modified successfully, restart to take effect!"
}

#Enable BBR+cake
startbbrcake () {
	remove_all
	echo "net.core.default_qdisc=cake" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBR+cake modified successfully, restart to take effect!"
}

#Enable BBRplus
startbbrplus () {
	remove_all
	echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbrplus" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBRplus modified successfully, restart to take effect!"
}

#Enable Lotserver
startlotserver(){
	remove_all
	if [[ "${release}" == "centos" ]]; then
		yum install ethtool
	else
		apt-get update
		apt-get install ethtool
	be
	#bash <(wget -qO- https://git.io/lotServerInstall.sh) install
	bash <(wget --no-check-certificate -qO- https://github.com/xidcn/LotServer_Vicer/raw/master/Install.sh) install
	sed -i '/advinacc/d' /appex/etc/config
	sed -i '/maxmode/d' /appex/etc/config
	echo -e "advinacc=\"1\"
maxmode=\"1\"">>/appex/etc/config
	/appex/bin/lotServer.sh restart
	start_menu
}

#Enable BBR2+FQ
startbbr2fq () {
	remove_all
	echo "net.ipv4.tcp_ecn=0" >> /etc/sysctl.conf
	echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbr2" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBR2 modified successfully, restart to take effect!"
}

#Enable BBR2+CAKE
startbbr2cake () {
	remove_all
	echo "net.ipv4.tcp_ecn=0" >> /etc/sysctl.conf
	echo "net.core.default_qdisc=cake" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbr2" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBR2 modified successfully, restart to take effect!"
}

#Enable BBR2+FQ+ecn
startbbr2fqecn () {
	remove_all
	echo "net.ipv4.tcp_ecn=1" >> /etc/sysctl.conf
	echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbr2" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBR2 modified successfully, restart to take effect!"
}

#Enable BBR2+CAKE+ecn
startbbr2cakeecn () {
	remove_all
	echo "net.ipv4.tcp_ecn=1" >> /etc/sysctl.conf
	echo "net.core.default_qdisc=cake" >> /etc/sysctl.conf
	echo "net.ipv4.tcp_congestion_control=bbr2" >> /etc/sysctl.conf
	sysctl -p
	echo -e "${Info}BBR2 modified successfully, restart to take effect!"
}


#Uninstall all speed up
remove_all(){
	rm -rf bbrmod
	But -i '/net.ipv4.tcp_retries2/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_slow_start_after_idle/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_fastopen/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_ecn/d' /etc/sysctl.conf
	But -i '/net.core.default_qdisc/d' /etc/sysctl.conf
    But -i '/net.ipv4.tcp_congestion_control/d' /etc/sysctl.conf
    But -i '/fs.file-max/d' /etc/sysctl.conf
	But -i '/net.core.rmem_max/d' /etc/sysctl.conf
	But -i '/net.core.wmem_max/d' /etc/sysctl.conf
	But -i '/net.core.rmem_default/d' /etc/sysctl.conf
	But -i '/net.core.wmem_default/d' /etc/sysctl.conf
	But -i '/net.core.netdev_max_backlog/d' /etc/sysctl.conf
	But -i '/net.core.somaxconn/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_syncookies/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_tw_reuse/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_tw_recycle/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_fin_timeout/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_keepalive_time/d' /etc/sysctl.conf
	But -i '/net.ipv4.ip_local_port_range/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_max_syn_backlog/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_max_tw_buckets/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_rmem/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_wmem/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_mtu_probing/d' /etc/sysctl.conf
	But -i '/net.ipv4.ip_forward/d' /etc/sysctl.conf
	But -i '/fs.inotify.max_user_instances/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_syncookies/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_fin_timeout/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_tw_reuse/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_max_syn_backlog/d' /etc/sysctl.conf
	But -i '/net.ipv4.ip_local_port_range/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_max_tw_buckets/d' /etc/sysctl.conf
	But -i '/net.ipv4.route.gc_timeout/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_synack_retries/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_syn_retries/d' /etc/sysctl.conf
	But -i '/net.core.somaxconn/d' /etc/sysctl.conf
	But -i '/net.core.netdev_max_backlog/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_timestamps/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_max_orphans/d' /etc/sysctl.conf
	if [[ -e /appex/bin/lotServer.sh ]]; then
		bash <(wget -qO- https://git.io/lotServerInstall.sh) uninstall
	be
	clear
	echo -e "${Info}: Clear acceleration is complete."
	sleep 1s
}

#Optimize system configuration
optimizing_system(){
	But -i '/net.ipv4.tcp_retries2/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_slow_start_after_idle/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_fastopen/d' /etc/sysctl.conf
	But -i '/fs.file-max/d' /etc/sysctl.conf
	But -i '/fs.inotify.max_user_instances/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_syncookies/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_fin_timeout/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_tw_reuse/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_max_syn_backlog/d' /etc/sysctl.conf
	But -i '/net.ipv4.ip_local_port_range/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_max_tw_buckets/d' /etc/sysctl.conf
	But -i '/net.ipv4.route.gc_timeout/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_synack_retries/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_syn_retries/d' /etc/sysctl.conf
	But -i '/net.core.somaxconn/d' /etc/sysctl.conf
	But -i '/net.core.netdev_max_backlog/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_timestamps/d' /etc/sysctl.conf
	But -i '/net.ipv4.tcp_max_orphans/d' /etc/sysctl.conf
	But -i '/net.ipv4.ip_forward/d' /etc/sysctl.conf
	echo "net.ipv4.tcp_retries2 = 8
net.ipv4.tcp_slow_start_after_idle = 0
fs.file-max = 1000000
fs.inotify.max_user_instances = 8192
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_fin_timeout = 30
net.ipv4.tcp_tw_reuse = 1
net.ipv4.ip_local_port_range = 1024 65000
net.ipv4.tcp_max_syn_backlog = 16384
net.ipv4.tcp_max_tw_buckets = 6000
net.ipv4.route.gc_timeout = 100
net.ipv4.tcp_syn_retries = 1
net.ipv4.tcp_synack_retries = 1
net.core.somaxconn = 32768
net.core.netdev_max_backlog = 32768
net.ipv4.tcp_timestamps = 0
net.ipv4.tcp_max_orphans = 32768
# forward ipv4
#net.ipv4.ip_forward = 1">>/etc/sysctl.conf
	sysctl -p
	echo "* soft nofile 1000000
*               hard    nofile          1000000">/etc/security/limits.conf
	echo "ulimit -SHn 1000000">>/etc/profile
	read -p "You need to restart the VPS for the system optimization configuration to take effect. Do you want to restart now? [Y/n] :" yn
	[ -z "${yn}" ] && yn="y"
	if [[$ yn == [Yy]]]; then
		echo -e "${Info} VPS is restarting..."
		reboot
	be
}

optimizing_system_johnrosen1()
{
cat > '/etc/sysctl.d/99-sysctl.conf' << EOF
#!!! Do not change these settings unless you know what you are doing !!!
#net.ipv4.ip_forward = 1
#net.ipv4.conf.all.forwarding = 1
#net.ipv4.conf.default.forwarding = 1
###############################
#net.ipv6.conf.all.forwarding = 1
#net.ipv6.conf.default.forwarding = 1
#net.ipv6.conf.lo.forwarding = 1
###############################
net.ipv6.conf.all.disable_ipv6 = 0
net.ipv6.conf.default.disable_ipv6 = 0
net.ipv6.conf.lo.disable_ipv6 = 0
###############################
net.ipv6.conf.all.accept_ra = 2
net.ipv6.conf.default.accept_ra = 2
###############################
net.core.netdev_max_backlog = 100000
net.core.netdev_budget = 50000
net.core.netdev_budget_usecs = 5000
#fs.file-max = 51200
net.core.rmem_max = 67108864
net.core.wmem_max = 67108864
net.core.rmem_default = 65536
net.core.wmem_default = 65536
net.core.somaxconn = 10000
###############################
net.ipv4.icmp_echo_ignore_all = 0
net.ipv4.icmp_echo_ignore_broadcasts = 1
net.ipv4.icmp_ignore_bogus_error_responses = 1
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.all.secure_redirects = 0
net.ipv4.conf.default.secure_redirects = 0
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0
net.ipv4.conf.default.rp_filter = 1
net.ipv4.conf.all.rp_filter = 1
net.ipv4.tcp_keepalive_time = 1200
net.ipv4.tcp_keepalive_intvl = 15
net.ipv4.tcp_keepalive_probes = 5
net.ipv4.tcp_synack_retries = 2
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_rfc1337 = 1
net.ipv4.tcp_timestamps = 1
#net.ipv4.tcp_tw_recycle = 0
net.ipv4.tcp_tw_reuse = 1
net.ipv4.tcp_fin_timeout = 15
net.ipv4.ip_local_port_range = 10000 65000
net.ipv4.tcp_max_tw_buckets = 2000000
# net.ipv4.tcp_fastopen = 3
net.ipv4.tcp_rmem = 4096 87380 67108864
net.ipv4.tcp_wmem = 4096 65536 67108864
net.ipv4.udp_rmem_min = 8192
net.ipv4.udp_wmem_min = 8192
net.ipv4.tcp_mtu_probing = 0
#############################
net.ipv4.conf.all.arp_ignore = 2
net.ipv4.conf.default.arp_ignore = 2
net.ipv4.conf.all.arp_announce = 2
net.ipv4.conf.default.arp_announce = 2
#############################
net.ipv4.tcp_autocorking = 0
net.ipv4.tcp_slow_start_after_idle = 0
net.ipv4.tcp_max_syn_backlog = 30000
net.core.default_qdisc = fq
net.ipv4.tcp_congestion_control = bbr
net.ipv4.tcp_notsent_lowat = 16384
net.ipv4.tcp_no_metrics_save = 1
net.ipv4.tcp_ecn = 2
net.ipv4.tcp_ecn_fallback = 1
net.ipv4.tcp_frto = 0
#############################
net.ipv6.conf.all.accept_redirects = 0
net.ipv6.conf.default.accept_redirects = 0
vm.swappiness = 0
net.ipv4.ip_unprivileged_port_start = 0
EOF
	sysctl --system
	cat > '/etc/systemd/system.conf' << EOF
[Manager]
#DefaultTimeoutStartSec=90s
DefaultTimeoutStopSec=30s
#DefaultRestartSec=100ms
DefaultLimitCORE=infinity
DefaultLimitNOFILE=51200
DefaultLimitNPROC=51200
EOF
		cat > '/etc/security/limits.conf' << EOF
* soft nofile 51200
* hard nofile 51200
* soft nproc 51200
* hard nproc 51200
EOF
if grep -q "ulimit" /etc/profile
then
	:
else
echo "ulimit -SHn 51200" >> /etc/profile
echo "ulimit -SHu 51200" >> /etc/profile
be
if grep -q "pam_limits.so" /etc/pam.d/common-session
then
	:
else
echo "session required pam_limits.so" >> /etc/pam.d/common-session
be
systemctl daemon-reload
}

#Update script
Update_Shell(){
	echo -e "The current version is [${sh_ver} ], start checking the latest version..."
	sh_new_ver=$(wget -qO- "https://${github}/tcp.sh"|grep 'sh_ver="'|awk -F "=" '{print $NF}'|sed 's/\"//g'|head -1)
	[[ -z ${sh_new_ver} ]] && echo -e "${Error} failed to detect the latest version!" && start_menu
	if [[ ${sh_new_ver} != ${sh_ver} ]]; then
		echo -e "Found a new version [${sh_new_ver} ], do you want to update it? [Y/n]"
		read -p "(默认: y):" yn
		[[ -z "${yn}" ]] && yn="y"
		if [[$ {in} == [Yy]]]; then
			wget -N "https://${github}/tcp.sh" && chmod +x tcp.sh && ./tcp.sh
			echo -e "The script has been updated to the latest version [${sh_new_ver}] !"
		else
			echo && echo "Cancelled..." && echo
		be
	else
		echo -e "Currently is the latest version [${sh_new_ver}] !"
		sleep 2s && ./tcp.sh
	be
}

#Switch to not uninstall the kernel version
gototcpx(){
	clear
	wget -N "https://github.000060000.xyz/tcpx.sh" && chmod +x tcpx.sh && ./tcpx.sh
}

#Switch to Qiu Shui Yi Bing BBR installation script
gototeddysun_bbr () {
	clear
	wget https://github.com/teddysun/across/raw/master/bbr.sh && chmod +x bbr.sh && ./bbr.sh
}

#Switch to one-click DD installation system script novices do not enter 
got () {
	clear
	wget -qO ~/Network-Reinstall-System-Modify.sh 'https://github.com/ylx2016/reinstall/raw/master/Network-Reinstall-System-Modify.sh' && chmod a+x ~/Network-Reinstall-System-Modify.sh && bash ~/Network-Reinstall-System-Modify.sh -UI_Options
}

#Start Menu
start_menu () {
clear
echo && echo -e "TCP accelerated one-key installation management script ${Red_font_prefix}[v${sh_ver}]${Font_color_suffix}
 Update content and feedback: https://blog.ylx.me/archives/783.html Run./tcp.sh call this script again and use it with caution
  
 ${Green_font_prefix}0.${Font_color_suffix} upgrade script
 ${Green_font_prefix}9.${Font_color_suffix} switch to non-uninstall kernel version
 ${Green_font_prefix}88.${Font_color_suffix} Switch to Qiushui Yibing BBR installation script
 ${Green_font_prefix}100.${Font_color_suffix} Switch to one-click DD installation system script at your own responsibility, novices do not enter 
————————————Kernel Management————————————
 ${Green_font_prefix}1.${Font_color_suffix} Install the original BBR kernel-5.6.15/5.10.0
 ${Green_font_prefix}2.${Font_color_suffix} Install BBRplus kernel-4.14.129
 ${Green_font_prefix}3.${Font_color_suffix} Install Lotserver (Sharp Speed) Kernel-Various
 ${Green_font_prefix}4.${Font_color_suffix} Install the xanmod version of the kernel-5.5.1/5.9.13
 ${Green_font_prefix}5.${Font_color_suffix} Install BBR2 beta kernel-5.4.0
 ${Green_font_prefix}7.${Font_color_suffix} Install the new BBRplus kernel-4.14.182
———————————— Accelerated Management————————————
 ${Green_font_prefix}11.${Font_color_suffix} Use BBR+FQ to accelerate
 ${Green_font_prefix}19.${Font_color_suffix} Use BBR+FQ_PIE to accelerate 
 ${Green_font_prefix}12.${Font_color_suffix} Use BBR+CAKE to accelerate 
 ${Green_font_prefix}13.${Font_color_suffix} Use BBRplus+FQ version to accelerate
 ${Green_font_prefix}14.${Font_color_suffix} Use Lotserver (sharp speed) to accelerate
 ${Green_font_prefix}15.${Font_color_suffix} Use BBR2+FQ to accelerate
 ${Green_font_prefix}16.${Font_color_suffix} Use BBR2+CAKE to accelerate
 ${Green_font_prefix}17.${Font_color_suffix} Use BBR2+FQ+ECN to accelerate
 ${Green_font_prefix}18.${Font_color_suffix} 使用BBR2+CAKE+ECN加速 
———————————— Miscellaneous Management————————————
 ${Green_font_prefix}21.${Font_color_suffix} Uninstall all acceleration
 ${Green_font_prefix}22.${Font_color_suffix} system configuration optimization
 ${Green_font_prefix}24.${Font_color_suffix} The optimization scheme of johnrosen1 is mutually exclusive with the above scheme and accelerated management without uninstall
 ${Green_font_prefix}23.${Font_color_suffix} Exit script 
————————————————————————————————" && echo

	check_status
	echo -e "The current kernel is: ${Font_color_suffix}${kernel_version_r}${Font_color_suffix}"
	if [[ ${kernel_status} == "noinstall" ]]; then
		echo -e "Current status: ${Green_font_prefix} is not installed ${Font_color_suffix} acceleration kernel ${Red_font_prefix} please install the kernel ${Font_color_suffix}"
	else
		echo -e "Current status: ${Green_font_prefix} installed ${Font_color_suffix} ${_font_prefix}${kernel_status}${Font_color_suffix} to accelerate the kernel, ${Green_font_prefix}${run_status}${Font_color_suffix}"
		
	be
	echo -e "The current congestion control algorithm is: ${Green_font_prefix}${net_congestion_control}${Font_color_suffix} The current queue algorithm is: ${Green_font_prefix}${net_qdisc}${Font_color_suffix} "
	
echo
read -p "Please enter a number:" num
case "$num" in
	0)
	Update_Shell
	;;
	1)
	check_sys_bbr
	;;
	2)
	check_sys_bbrplus
	;;
	3)
	check_sys_Lotsever
	;;
	4)
	check_sys_xanmod
	;;
	5)
	check_sys_bbr2
	;;
	6)
	check_sys_zen
	;;
	7)
	check_sys_bbrplusnew	
	;;
	88)
	gototeddysun_bbr
	;;
	9)
	gototcpx
	;;
	100)
	he got	
	;;
	11)
	startbbrfq
	;;
	19)
	startbbrfqpie	
	;;
	12)
	startbbrcake
	;;
	13)
	startbbrplus
	;;
	14)
	startlotserver
	;;
	15)
	startbbr2fq
	;;
	16)
	startbbr2cake
	;;
	17)
	startbbr2fqecn
	;;
	18)
	startbbr2cakeecn
	;;
	21)
	remove_all
	;;
	22)
	optimizing_system
	;;
	24)
	optimizing_system_johnrosen1
	;;
	23)
	exit 1
	;;
	*)
	clear
	echo -e "${Error}: Please enter the correct number [0-23]"
	sleep 5s
	start_menu
	;;
esac
}
#############Core Management Components#############

#Delete redundant cores
detele_kernel () {
	if [[ "${release}" == "centos" ]]; then
		rpm_total=`rpm -qa | grep kernel | grep -v "${kernel_version}" | grep -v "noarch" | wc -l`
		if [ "${rpm_total}" > "1" ]; then
			echo -e "${rpm_total} remaining cores detected, start uninstalling..."
			for((integer = 1; integer <= ${rpm_total}; integer++)); do
				rpm_del=`rpm -qa | grep kernel | grep -v "${kernel_version}" | grep -v "noarch" | head -${integer}`
				echo -e "Start uninstalling the ${rpm_del} kernel..."
				rpm --nodeps -e $ {rpm_del
				echo -e "Uninstall ${rpm_del} kernel uninstall complete, continue..."
			done
			echo --nodeps -e "The kernel is uninstalled, continue..."
		else
			echo -e "The number of cores detected is incorrect, please check!" && exit 1
		be
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		deb_total=`dpkg -l | grep linux-image | awk '{print $2}' | grep -v "${kernel_version}" | wc -l`
		if [ "${deb_total}" > "1" ]; then
			echo -e "${deb_total} remaining cores detected, start uninstalling..."
			for((integer = 1; integer <= ${deb_total}; integer++)); do
				deb_del=`dpkg -l|grep linux-image | awk '{print $2}' | grep -v "${kernel_version}" | head -${integer}`
				echo -e "Start to uninstall the ${deb_del} kernel..."
				apt-get purge -y $ {deb_del}
				echo -e "Uninstall ${deb_del} kernel uninstall complete, continue..."
			done
			echo -e "The kernel is uninstalled, continue..."
		else
			echo -e "The number of cores detected is incorrect, please check!" && exit 1
		be
	be
}

detele_kernel_head () {
	if [[ "${release}" == "centos" ]]; then
		rpm_total=`rpm -qa | grep kernel-headers | grep -v "${kernel_version}" | grep -v "noarch" | wc -l`
		if [ "${rpm_total}" > "1" ]; then
			echo -e "Detected ${rpm_total} remaining head cores, start to uninstall..."
			for((integer = 1; integer <= ${rpm_total}; integer++)); do
				rpm_del=`rpm -qa | grep kernel-headers | grep -v "${kernel_version}" | grep -v "noarch" | head -${integer}`
				echo -e "Start uninstalling the kernel of ${rpm_del} headers..."
				rpm --nodeps -e $ {rpm_del
				echo -e "Uninstall ${rpm_del} kernel uninstall complete, continue..."
			done
			echo --nodeps -e "The kernel is uninstalled, continue..."
		else
			echo -e "The number of cores detected is incorrect, please check!" && exit 1
		be
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		deb_total=`dpkg -l | grep linux-headers | awk '{print $2}' | grep -v "${kernel_version}" | wc -l`
		if [ "${deb_total}" > "1" ]; then
			echo -e "Detected ${deb_total} remaining head cores, start to uninstall..."
			for((integer = 1; integer <= ${deb_total}; integer++)); do
				deb_del=`dpkg -l|grep linux-headers | awk '{print $2}' | grep -v "${kernel_version}" | head -${integer}`
				echo -e "Start uninstalling the ${deb_del} headers kernel..."
				apt-get purge -y $ {deb_del}
				echo -e "Uninstall ${deb_del} kernel uninstall complete, continue..."
			done
			echo -e "The kernel is uninstalled, continue..."
		else
			echo -e "The number of cores detected is incorrect, please check!" && exit 1
		be
	be
}




#Update Guide
BBR_grub(){
	if [[ "${release}" == "centos" ]]; then
        if [[ ${version} = "6" ]]; then
            if [ ! -f "/boot/grub/grub.conf" ]; then
                echo -e "${Error} /boot/grub/grub.conf not found, please check."
                exit 1
            be
            sed -i 's/^default=.*/default=0/g' /boot/grub/grub.conf
        elif [[ ${version} = "7" ]]; then
            if [ -f "/boot/grub2/grub.cfg" ]; then
				grub2-mkconfig  -o   /boot/grub2/grub.cfg
				grub2-set-default 0
				exit 1
			elif [ -f "/boot/efi/EFI/centos/grub.cfg" ]; then
				grub2-mkconfig  -o   /boot/efi/EFI/centos/grub.cfg
				grub2-set-default 0
				exit 1
			else
				echo -e "${Error} grub.cfg cannot be found, please check."
            be
			#grub2-mkconfig  -o   /boot/grub2/grub.cfg
			#grub2-set-default 0
		
		elif [[ ${version} = "8" ]]; then
			grub2-mkconfig  -o   /boot/grub2/grub.cfg
			grubby --info=ALL|awk -F= '$1=="kernel" {print i++ " : " $2}'
        be
    elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
        /usr/sbin/update-grub
		#exit 1
    be
}

#############Core Management Components#############



############System Testing Components#############

#Check system
check_sys(){
	if [[ -f /etc/redhat-release ]]; then
		release="centos"
	elif cat /etc/issue | grep -q -E -i "debian"; then
		release="debian"
	elif cat /etc/issue | grep -q -E -i "ubuntu"; then
		release="ubuntu"
	elif cat /etc/issue | grep -q -E -i "centos|red hat|redhat"; then
		release="centos"
	elif cat /proc/version | grep -q -E -i "debian"; then
		release="debian"
	elif cat /proc/version | grep -q -E -i "ubuntu"; then
		release="ubuntu"
	elif cat /proc/version | grep -q -E -i "centos|red hat|redhat"; then
		release="centos"
    be
	
#Handle ca certificate
	if [[ "${release}" == "centos" ]]; then
		yum install ca-certificates -y
		update-ca-trust force-enable
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		apt-get install ca-certificates -y
		update-ca-certificates
	be	
}

#Check Linux version
check_version(){
	if [[ -s /etc/redhat-release ]]; then
		version=`grep -oE  "[0-9.]+" /etc/redhat-release | cut -d . -f 1`
	else
		version=`grep -oE  "[0-9.]+" /etc/issue | cut -d . -f 1`
	be
	bit=`uname -m`
	if [[ ${bit} = "x86_64" ]]; then
		bit="x64"
	else
		bit="x32"
	be
}

#Check the system requirements for installing bbr
check_sys_bbr(){
	check_version
	if [[ "${release}" == "centos" ]]; then
		# if [[ ${version} = "6" || ${version} = "7" || ${version} = "8" ]]; then
		if [[ ${version} = "7" || ${version} = "8" ]]; then
			installbbr
		else
			echo -e "${Error} BBR kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		be
	# elif [[ "${release}" == "debian" ]]; then
		# if [[ ${version} = "8" || ${version} = "9" || ${version} = "10" ]]; then
		# if [[ ${version} = "9" || ${version} = "10" ]]; then
			# installbbr
		# else
			# echo -e "${Error} BBR kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		# fi
	# elif [[ "${release}" == "ubuntu" ]]; then
		# if [[ ${version} = "16" || ${version} = "18" || ${version} = "19" || ${version} = "20" ]]; then
		# if [[ ${version} = "16" || ${version} = "18" || ${version} = "20" ]]; then
			# installbbr
		# else
			# echo -e "${Error} BBR kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		# fi
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		# if [[ ${version} = "8" || ${version} = "9" || ${version} = "10" || ${version} = "16" || ${version} = "18" || ${version} = "19" || ${version} = "20" ]]; then
			installbbr
		# fi
	else
		echo -e "${Error} BBR kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	be
}

check_sys_bbrplus(){
	check_version
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" ]]; then
			installbbrplus
		else
			echo -e "${Error} BBRplus kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		be
	# elif [[ "${release}" == "debian" ]]; then
		# if [[ ${version} = "8" || ${version} = "9" || ${version} = "10" ]]; then
		# if [[ ${version} = "9" || ${version} = "10" ]]; then
			# installbbrplus
		# else
			# echo -e "${Error} BBRplus kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		# fi
	# elif [[ "${release}" == "ubuntu" ]]; then
		# if [[ ${version} = "16" || ${version} = "18" || ${version} = "19" ]]; then
		# if [[ ${version} = "16" || ${version} = "18" ]]; then
			# installbbrplus
		# else
			# echo -e "${Error} BBRplus kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		# fi
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		# if [[ ${version} = "8" || ${version} = "9" || ${version} = "10" || ${version} = "16" || ${version} = "18" || ${version} = "19" || ${version} = "20" ]]; then
			installbbrplus
		# fi	
	else
		echo -e "${Error} BBRplus kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	be
}

check_sys_bbrplusnew(){
	check_version
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" ]]; then
			installbbrplusnew
		else
			echo -e "${Error} BBRplusNew kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		be
	# elif [[ "${release}" == "debian" ]]; then
		# if [[ ${version} = "10" ]]; then
			# installbbrplusnew
		# else
			# echo -e "${Error} BBRplusNew kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		# fi
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		# if [[ ${version} = "8" || ${version} = "9" || ${version} = "10" || ${version} = "16" || ${version} = "18" || ${version} = "19" || ${version} = "20" ]]; then
			installbbrplusnew
	else
		echo -e "${Error} BBRplusNew kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	be
}

check_sys_xanmod(){
	check_version
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" || ${version} = "8" ]]; then
			installxanmod
		else
			echo -e "${Error} xanmod kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		be
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		# if [[ ${version} = "8" || ${version} = "9" || ${version} = "10" || ${version} = "16" || ${version} = "18" || ${version} = "19" || ${version} = "20" ]]; then
			installxanmod
		# fi
	# elif [[ "${release}" == "ubuntu" ]]; then
			# echo -e "${Error} xanmod kernel does not support the current system ${release} ${version} ${bit}, go to xanmod.org official website to install!" && exit 1
	else
		echo -e "${Error} xanmod kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	be
}

check_sys_bbr2(){
	check_version
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" || ${version} = "8" ]]; then
			installbbr2
		else
			echo -e "${Error} bbr2 kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		be
	# elif [[ "${release}" == "debian" ]]; then
		# if [[ ${version} = "9" || ${version} = "10" ]]; then
			# installbbr2
		# else
			# echo -e "${Error} bbr2 kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		# fi
	# elif [[ "${release}" == "ubuntu" ]]; then
			# echo -e "${Error} bbr2 kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		# if [[ ${version} = "8" || ${version} = "9" || ${version} = "10" || ${version} = "16" || ${version} = "18" || ${version} = "19" || ${version} = "20" ]]; then
			installbbr2		
	else
		echo -e "${Error} bbr2 kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	be
}


check_sys_zen(){
	check_version
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} = "7" || ${version} = "8" ]]; then
			installzen
		else
			echo -e "${Error} zen kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		be
	# elif [[ "${release}" == "debian" ]]; then
		# if [[ ${version} = "9" || ${version} = "10" ]]; then
			# installzen
		# else
			# echo -e "${Error} zen kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
		# fi
	# elif [[ "${release}" == "ubuntu" ]]; then
			# echo -e "${Error} zen kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	elif [[ "${release}" == "debian" || "${release}" == "ubuntu" ]]; then
		# if [[ ${version} = "8" || ${version} = "9" || ${version} = "10" || ${version} = "16" || ${version} = "18" || ${version} = "19" || ${version} = "20" ]]; then
			installzen				
	else
		echo -e "${Error} zen kernel does not support the current system ${release} ${version} ${bit} !" && exit 1
	be
}

#Check the system requirements for installing Lotsever
check_sys_Lotsever(){
	check_version
	if [[ "${release}" == "centos" ]]; then
		if [[ ${version} == "6" ]]; then
			kernel_version="2.6.32-504"
			installlot
		elif [[ ${version} == "7" ]]; then
			yum -y install net-tools
			kernel_version="4.11.2-1"
			installlot
		else
			echo -e "${Error} Lotsever does not support the current system ${release} ${version} ${bit} !" && exit 1
		be
	elif [[ "${release}" == "debian" ]]; then
		if [[ ${version} = "7" || ${version} = "8" ]]; then
			if [[ ${bit} == "x64" ]]; then
				kernel_version="3.16.0-4"
				installlot
			elif [[ ${bit} == "x32" ]]; then
				kernel_version="3.2.0-4"
				installlot
			be
		elif [[ ${version} = "9" ]]; then
			if [[ ${bit} == "x64" ]]; then
				kernel_version="4.9.0-4"
				installlot
			be
		else
			echo -e "${Error} Lotsever does not support the current system ${release} ${version} ${bit} !" && exit 1
		be
	elif [[ "${release}" == "ubuntu" ]]; then
		if [[ ${version} -ge "12" ]]; then
			if [[ ${bit} == "x64" ]]; then
				kernel_version="4.4.0-47"
				installlot
			elif [[ ${bit} == "x32" ]]; then
				kernel_version="3.13.0-29"
				installlot
			be
		else
			echo -e "${Error} Lotsever does not support the current system ${release} ${version} ${bit} !" && exit 1
		be
	else
		echo -e "${Error} Lotsever does not support the current system ${release} ${version} ${bit} !" && exit 1
	be
}

check_status(){
	kernel_version=`uname -r | awk -F "-" '{print $1}'`
	kernel_version_full=`uname -r`
	net_congestion_control=`cat /proc/sys/net/ipv4/tcp_congestion_control | awk '{print $1}'`
	net_qdisc=`cat /proc/sys/net/core/default_qdisc | awk '{print $1}'`
	kernel_version_r=`uname -r | awk '{print $1}'`
	if [[ ${kernel_version_full} = "4.14.182-bbrplus" || ${kernel_version_full} = "4.14.168-bbrplus" || ${kernel_version_full} = "4.14.98-bbrplus" || ${kernel_version_full} = "4.14.129-bbrplus" || ${kernel_version_full} = "4.14.160-bbrplus" || ${kernel_version_full} = "4.14.166-bbrplus" || ${kernel_version_full} = "4.14.161-bbrplus" ]]; then
		kernel_status="BBRplus"
	elif [[ ${kernel_version} = "3.10.0" || ${kernel_version} = "3.16.0" || ${kernel_version} = "3.2.0" || ${kernel_version} = "4.4.0" || ${kernel_version} = "3.13.0"  || ${kernel_version} = "2.6.32" || ${kernel_version} = "4.9.0" || ${kernel_version} = "4.11.2" ]]; then
		kernel_status="Lotserver"
	elif [[ `echo ${kernel_version} | awk -F'.' '{print $1}'` == "4" ]] && [[ `echo ${kernel_version} | awk -F'.' '{print $2}'` -ge 9 ]] || [[ `echo ${kernel_version} | awk -F'.' '{print $1}'` == "5" ]]; then
		kernel_status="BBR"
	else 
		kernel_status="noinstall"
	be
	

	if [[ ${kernel_status} == "BBR" ]]; then
		run_status=`cat /proc/sys/net/ipv4/tcp_congestion_control | awk '{print $1}'`
		if [[ ${run_status} == "bbr" ]]; then
			run_status=`cat /proc/sys/net/ipv4/tcp_congestion_control | awk '{print $1}'`
			if [[ ${run_status} == "bbr" ]]; then
				run_status="BBR started successfully"
			else 
				run_status="BBR failed to start"
			be
		elif [[ ${run_status} == "bbr2" ]]; then
			run_status=`cat /proc/sys/net/ipv4/tcp_congestion_control | awk '{print $1}'`
			if [[ ${run_status} == "bbr2" ]]; then
				run_status="BBR2 started successfully"
			else 
				run_status="BBR2 failed to start"
			be	
		elif [[ ${run_status} == "tsunami" ]]; then
			run_status=`lsmod | grep "tsunami" | awk '{print $1}'`
			if [[ ${run_status} == "tcp_tsunami" ]]; then
				run_status="BBR Magic Revamp started successfully"
			else 
				run_status="BBR magic revision failed to start"
			be
		elif [[ ${run_status} == "nanqinlang" ]]; then
			run_status=`lsmod | grep "nanqinlang" | awk '{print $1}'`
			if [[ ${run_status} == "tcp_nanqinlang" ]]; then
				run_status="The violent BBR magic revision started successfully"
			else 
				run_status="The violent BBR magic revision failed to start"
			be
		else 
			run_status="Acceleration module not installed"
		be
		
	elif [[ ${kernel_status} == "Lotserver" ]]; then
		if [[ -e /appex/bin/lotServer.sh ]]; then
			run_status=`bash /appex/bin/lotServer.sh status | grep "LotServer" | awk  '{print $3}'`
			if [[ ${run_status} = "running!" ]]; then
				run_status="Started successfully"
			else 
				run_status="Start failed"
			be
		else 
			run_status="Acceleration module not installed"
		be	
	elif [[ ${kernel_status} == "BBRplus" ]]; then
		run_status=`cat /proc/sys/net/ipv4/tcp_congestion_control | awk '{print $1}'`
		if [[ ${run_status} == "bbrplus" ]]; then
			run_status=`cat /proc/sys/net/ipv4/tcp_congestion_control | awk '{print $1}'`
			if [[ ${run_status} == "bbrplus" ]]; then
				run_status="BBRplus started successfully"
			else 
				run_status="BBRplus failed to start"
			be
		else 
			run_status="Acceleration module not installed"
		be
	be
}

############System Testing Components#############
check_sys
check_version
[[ ${release} != "debian" ]] && [[ ${release} != "ubuntu" ]] && [[ ${release} != "centos" ]] && echo -e "${Error} This script does not support the current system ${release} !" && exit 1
start_menu
