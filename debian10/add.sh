#!/bin/bash

# detail nama perusahaan
country=ID
state=DKIJakarta
locality=JakartaPusat
organization=RedSSH.Net
organizationalunit=RedSSH.Net
commonname=redssh.net
email=admin@redssh.net

# disable ipv6
echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6
sed -i '$ i\echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6' /etc/rc.local

# go to root
cd
# update
apt-get install net-tools
apt-get -y update

# set repo webmin
sh -c 'echo "deb http://download.webmin.com/download/repository sarge contrib" > /etc/apt/sources.list.d/webmin.list'
wget -qO - http://www.webmin.com/jcameron-key.asc | apt-key add -

# setting dan install vnstat debian 9 64bit
apt-get -y install vnstat
systemctl start vnstat
systemctl enable vnstat
chkconfig vnstat on

# install squid3
cd
apt-get -y install squid3
wget -O /etc/squid/squid.conf "https://gitlab.com/presult77/autoscript/-/raw/master/debian10/squid3.conf"
/etc/init.d/squid restart

# install webmin
cd
wget https://nchc.dl.sourceforge.net/project/webadmin/webmin/1.910/webmin_1.910_all.deb
dpkg --install webmin_1.910_all.deb;
apt-get -y -f install;
sed -i 's/ssl=1/ssl=0/g' /etc/webmin/miniserv.conf
rm -f webmin_1.910_all.deb
/etc/init.d/webmin restart

echo "=================  install stunnel  ====================="
echo "========================================================="

# install stunnel
apt-get install stunnel4 -y
cat > /etc/stunnel/stunnel.conf <<-END
cert = /etc/stunnel/stunnel.pem
client = no
socket = a:SO_REUSEADDR=1
socket = l:TCP_NODELAY=1
socket = r:TCP_NODELAY=1
[openvpnssl]
accept = 833
connect = 127.0.0.1:1194

END

echo "=================  membuat Sertifikat OpenSSL ======================"
echo "========================================================="
#membuat sertifikat
openssl genrsa -out key.pem 2048
openssl req -new -x509 -key key.pem -out cert.pem -days 1095 \
-subj "/C=$country/ST=$state/L=$locality/O=$organization/OU=$organizationalunit/CN=$commonname/emailAddress=$email"
cat key.pem cert.pem >> /etc/stunnel/stunnel.pem

# konfigurasi stunnel
sed -i 's/ENABLED=0/ENABLED=1/g' /etc/default/stunnel4
/etc/init.d/stunnel4 restart
chown -R vnstat:vnstat /var/lib/vnstat

cd
sed -i '$ i\0 */12 * * * root /usr/bin/fix' /etc/crontab

cd /usr/bin
wget -O fix "https://gitlab.com/presult77/autoscript/-/raw/master/debian10/fix.sh"
chmod +x fix

sudo apt-get install gnupg1 apt-transport-https dirmngr -y
export INSTALL_KEY=379CE192D401AB61
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys $INSTALL_KEY
echo "deb https://ookla.bintray.com/debian generic main" | sudo tee  /etc/apt/sources.list.d/speedtest.list
sudo apt-get update -y
sudo apt-get install speedtest -y
